﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Movement : MonoBehaviour
{
    private Rotate rotation;
    private Check checker;
    private WorldRotation world;

    private Vector3 lastPosition;

    //public void Execute(GameObject gameObject)
    //{
    //    gameObject.transform.Translate(gameObject.transform.forward);
    //}

    private void Awake()
    {
        checker = GetComponent<Check>();
        rotation = GetComponent<Rotate>();

        lastPosition = transform.position;
    }

    private void Update()
    {
        if (checker.GroundChecker())
        {
            lastPosition = transform.position;

            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");

            transform.Translate(x * 0.1f, 0, y * 0.1f, Space.World);
            transform.LookAt(transform.position + new Vector3(x, 0, y));
        }
        else
        {
            transform.position = lastPosition;
            //rotation.RotateForward();
        }
    }
}
