﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    private float timeSpawn = 10f;
    public GameObject[] enemies;

    BoxCollider collider;
    Bounds spawnB;

    private float currentTimer = 0f;
    public bool ok;

    private void Start()
    {
        ok = true;
        collider = GetComponent<BoxCollider>();

        if (collider == null)
        {
            ok = false;
        }
        else
        {
            spawnB = collider.bounds;
        }

        if (enemies == null || enemies.Length == 0)
        {
            ok = false;
        }
    }

    private void Update()
    {
        if(ok)
        {
            currentTimer += Time.deltaTime;

            if (currentTimer >= timeSpawn)
            {
                currentTimer = 0;

                float spawnX = Random.Range(spawnB.extents.x, spawnB.extents.x);
                float spawnY = transform.position.y;
                float spawnZ = transform.position.z;

                GameObject newItem = Object.Instantiate(GetPrefab()) as GameObject;
                newItem.transform.position = new Vector3(spawnX, spawnY, spawnZ);
            }
        }
    }

    private GameObject GetPrefab()
    {
        int index = Random.Range(0, enemies.Length);

        return enemies[index];
    }
}
